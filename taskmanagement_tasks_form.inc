<?php
/**
 * @file
 * Tasks form
 */

/**
 * Build the tasks form
 */
function taskmanagement_tasks_form($form_state) {
  // Identify that the elements in 'example_items' are a collection, to
  // prevent Form API from flattening the array when submitted.
  $form['tasks']['#tree'] = TRUE;

  // Fetch the example data from the database, ordered by parent/child/weight.
  extract(_taskmanagement_get_task_tree());
  _taskmanagement_generate_tasks_form($form, $nodes, $child_mapping);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['task_add'] = array(
    '#markup' => l(
      t('Add Task'),
      "node/add/taskmanagement-task",
      array('query' => array('destination' => 'taskmanagement/tasks'))
    ),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
    '#submit' => array('taskmanagement_tasks_form_save_changes'),
  );

  return $form;
}

/**
 * Submit callback for the tasks form
 */
function taskmanagement_tasks_form_save_changes($form, &$form_state) {
  foreach ($form_state['values']['tasks'] as $id => $task) {
    db_update('taskmanagement_task')
      ->fields(array(
        'pid' => (int)$task['pid'],
        'weight' => (int)$task['weight'],
      ))
      ->condition('nid', (int)$task['nid'])
      ->execute();
  }
}

/**
 * Theme callback for the tasks form
 */
function theme_taskmanagement_tasks_form($variables) {
  $form = $variables['form'];

  $table_id = 'tasks-table';

  $header = array(
    t('Title'), 
    array('data' => 'weight', 'class' => array('taskmanagement-hidden')),
    array('data' => 'nid', 'class' => array('taskmanagement-hidden')),
    array('data' => 'pid', 'class' => array('taskmanagement-hidden')),
    '',
  );

  $rows = array();
  foreach (element_children($form['tasks']) as $id) {

    $indent = theme('indentation', array('size' => $form['tasks'][$id]['depth']['#value']));
    unset ($form['tasks'][$id]['depth']);

    $rows[] = array(
      'data' => array(
        $indent . drupal_render($form['tasks'][$id]['title']),
        array( // TODO: need these be placed inside the table?
          'data' => drupal_render($form['tasks'][$id]['weight']),
          'class' => array('taskmanagement-hidden'),
        ),
        array(
          'data' => drupal_render($form['tasks'][$id]['nid']),
          'class' => array('taskmanagement-hidden'),
        ),
        array(
          'data' => drupal_render($form['tasks'][$id]['pid']),
          'class' => array('taskmanagement-hidden'),
        ),
        drupal_render($form['tasks'][$id]['add_child']) . ' ' . 
        drupal_render($form['tasks'][$id]['edit_task']) . ' ' .
        drupal_render($form['tasks'][$id]['delete_task']),
      ),
      'class' => array('draggable'),
    );
  }

  $output = drupal_render($form['actions']['task_add']);
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $table_id)));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag($table_id, 'match', 'parent', 'task-pid', 'task-pid', 'task-nid', FALSE);
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'task-weight', NULL, NULL, FALSE);

  drupal_add_css(".taskmanagement-hidden {display: none}", 'inline');

  return $output;
}

function _taskmanagement_generate_tasks_form(&$form, $nodes, $child_mapping, $root_id = 0, $depth = 0) {
  if (!isset($child_mapping[$root_id])) return;
  foreach ($child_mapping[$root_id] as $child_id) { // TODO extract tree traversal TODO might put Tree stuff in a separate module, could that ever be made useful to others too? Actually, it probably already exists, outside drupal. Probably just leave it as is, not make module or such, maybe look up a Tree thing for php online
    $child = $nodes[$child_id];
    $form['tasks'][$child_id] = array(

      'title' => array(
        '#markup' => l($child->title, "node/$child_id", array(
          'query' => array('destination' => 'taskmanagement/tasks'),
        )),
      ),

      'nid' => array(
        '#type' => 'hidden',
        '#default_value' => $child_id,
        '#disabled' => TRUE,
        '#attributes' => array('class' => array('task-nid')),
      ),

      'pid' => array(
        '#type' => 'hidden',
        '#default_value' => $root_id,
        '#attributes' => array('class' => array('task-pid')),
      ),

      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $child->weight,
        '#delta' => 10,
        '#title-display' => 'invisible',
        '#attributes' => array('class' => array('task-weight')),
      ),

      'add_child' => array(
        '#markup' => l(
          theme('image', array('path' => drupal_get_path('module', 'taskmanagement') . '/images/add.gif')),
          "node/add/taskmanagement-task/$child_id",
          array(
            'query' => array('destination' => 'taskmanagement/tasks'),
            'html' => TRUE,
        )),
      ),

      'edit_task' => array(
        '#markup' => l(
          theme('image', array('path' => drupal_get_path('module', 'taskmanagement') . '/images/edit.gif')),
          "node/$child_id/edit",
          array(
            'query' => array('destination' => 'taskmanagement/tasks'),
            'html' => TRUE,
        )),
      ),

      'delete_task' => array(
        '#markup' => l(
          theme('image', array('path' => drupal_get_path('module', 'taskmanagement') . '/images/delete.png')),
          "node/$child_id/delete",
          array(
            'query' => array('destination' => 'taskmanagement/tasks'),
            'html' => TRUE,
        )),
      ),

      'depth' => array(
        '#type' => 'hidden',
        '#value' => $depth,
      ),
    );

    _taskmanagement_generate_tasks_form($form, $nodes, $child_mapping, $child_id, $depth+1);
  }
}

